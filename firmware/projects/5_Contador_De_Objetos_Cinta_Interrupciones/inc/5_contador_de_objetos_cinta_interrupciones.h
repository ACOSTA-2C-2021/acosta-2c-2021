/*! @mainpage Contador_de_objetos_cinta_interrupciones
 *
 * \section genDesc General Description
 *
 *Proyecto 2: Contador de objetos en cinta transportadora con interrupciones
 *Cuenta  objetos en una cinta transportadora mediante el sensor Tcrt 5000, utiliza el driver tcrt5000 y DisplayITS_E0803.
 *Muestra la cantidad de objetos contados utilizando los leds como un contador binario:
 *el LED_RGB_B (Azul) como el b3, el LED_1 como b2, el LED_2 como b1y el LED_3 como b0.
 *Muestra la cantidad de objetos contados utilizando el display LCD.
 *Se utilizan las teclas:TEC1 para activar y detener el conteo, TEC2 para mantener el resultado HOLD y TEC3 para resetear el conteo.
 *
 * \section hardConn Hardware Connection
 *
 * | ITS_E0803		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND 		| 	GND			|
 *
 * | TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	T_COL0		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND	 		| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * | 27/09/2021	| terminado                                      |
 *
 * @author Acosta Azul
 *
 */

#ifndef _5_CONTADOR_DE_OBJETOS_CINTA_INTERRUPCIONES_H
#define _5_CONTADOR_DE_OBJETOS_CINTA_INTERRUPCIONES_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _5_CONTADOR_DE_OBJETOS_CINTA_INTERRUPCIONES_H */

