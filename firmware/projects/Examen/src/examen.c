/*! @mainpage Examen
 *
 * \section genDesc General Description
 *
 *Se pretende diseñar un dispositivo que se utilizará como detector de objetos cercanos (sonar) en un robot móvil.
 *Dicho dispositivo cuenta con un HC-SR04 para el sensado de los objetos y es ubicado en distintas
 *orientaciones por medio de un motor.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | HC_SR4			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	5V			|
 * | 	GND		 	| 	GND			|
 * | 	ECHO	 	| 	GPIO_T_FIL2	|
 * | 	TRIGGER		| 	GPIO_T_FIL0	|
 *
 * | POTENCIOMETRO	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	3.3V		|
 * | 	GND		 	| 	GND			|
 * | 	OUT		 	| 	CH1			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Azul Acosta
 *
 */

/*==================[inclusions]=============================================*/
#include "examen.h"       /* <= own header */
#include "hc_sr4.h"
#include "timer.h"
#include "uart.h"
#include "led.h"
#include "gpio.h"
#include "goniometro.h"
#include "systemclock.h"

/*==================[macros and definitions]=================================*/
#define ANGULOIZQ 0 /*Angulo para una tension de 1.2v*/
#define ANGULODER 75/*Angulo para una tension de 2.1v*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

void DatosAPC();
void MedicionObjetos();
void SystemInit();

/*==================[external data definition]===============================*/
timer_config my_timer={TIMER_A,1000,&MedicionObjetos};
serial_config my_uart_pc={SERIAL_PORT_PC, 115200, &DatosAPC};
gpio_t echo_g={GPIO_T_FIL2};
gpio_t trigger_g={GPIO_T_FIL0};

uint16_t angulo_aux;
uint16_t distancia_cm_aux;
uint16_t minimo_aux=0;
uint32_t vector_grados[]={0,15,30,45,60,75};
uint16_t i=0;
uint32_t angulo_distancia_minima;

/*==================[external functions definition]==========================*/
/*Funcion de interrupcion para la uart*/

void DatosAPC(){

	/*[grado]º objeto a [distancia] cm*/
	UartSendString(SERIAL_PORT_PC, UartItoa(vector_grados[0],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"objeto");
	UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm_aux[0],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"cm");
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa(vector_grados[1],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"objeto");
	UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm_aux[1],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"cm");
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa(vector_grados[2],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"objeto");
	UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm_aux[2],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"cm");
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa(vector_grados[3],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"objeto");
	UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm_aux[3],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"cm");
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa(vector_grados[4],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"objeto");
	UartSendString(SERIAL_PORT_PC, UartItoa(distancia_cm_aux[4],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"cm");
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");
	/*Obstáculo en 15º (por ejemplo si a los 15º se obtuvo la menor medición de distancia).*/
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"Obstáculo en");
	UartSendString(SERIAL_PORT_PC, UartItoa(angulo_distancia_minima,10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");




}
/*Funcion de interrupcion de timer, cada 1 segundo va a
 * preguntar si el angulo es igual a alguno de los 5 angulos
 * que están en el vector_grados, son 5
 * porque tiene que recorrer 75 grados y cada 15 grados hacer la medicion,
 * por lo que dentro de 75 grados hay 5 veces 15 grados
 * angulo_aux es el valor que me devuelve la funcion DatoAngulo del driver goniometro*/

void MedicionObjetos(){
for(i=0;i<5;i++){
	if(angulo_aux==vector_grados[i]){
			distancia_cm_aux[i]=HcSr04ReadDistanceCentimeters();
		}
	if(distancia_cm_aux[i]<minimo_aux){
			 minimo_aux=distancia_cm_aux[i];
			 angulo_distancia_minima=vector_grados[i];

		 }
}


}
/*Funcion de inicializacion*/

void SystemInit(){
	SystemClockInit();
	LedsInit();
	HcSr04Init(echo_g, trigger_g); /*Inicializa sensor*/
	GoniometroInit();
	UartInit(&my_uart_pc);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
}
int main(void){

	SystemInit();

    while(1){

    	DatoAngulo(&angulo_aux); /*Funcion de driver goniometro que me devuelve el angulo*/
    	/*Control motor*/
    	if(angulo_aux==ANGULOIZQ){
    		LedOn(LED_RGB_R); /*Led rojo motor girando hacia la derecha*/
    	}
    	if(angulo_aux==ANGULODER){
    		LedOn(LED_RGB_G);/*Led verde motor girando hacia la izquierda*/
    	}
    	}

    LedOff(LED_RGB_R);
    LedOff(LED_RGB_G);
	return 0;
}

/*==================[end of file]============================================*/

