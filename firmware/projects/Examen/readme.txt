﻿Descripción del Proyecto.
 *Se pretende diseñar un dispositivo que se utilizará como detector de objetos cercanos (sonar) en un robot móvil.
 *Dicho dispositivo cuenta con un HC-SR04 para el sensado de los objetos y es ubicado en distintas
 *orientaciones por medio de un motor.
Conexionado de hardware
* | HC_SR4			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	5V			|
 * | 	GND		 	| 	GND			|
 * | 	ECHO	 	| 	GPIO_T_FIL2	|
 * | 	TRIGGER		| 	GPIO_T_FIL0	|
 *
 * | POTENCIOMETRO	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	3.3V			|
 * | 	GND		 	| 	GND			|
 * | 	OUT		 	| 	CH1			|
 * 