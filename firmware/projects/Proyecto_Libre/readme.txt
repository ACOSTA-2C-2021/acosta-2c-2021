﻿Descripción del Proyecto.
* Esta aplicación simula la automatizacion de luces y persianas en un aula.
 *Dependiendo de la cantidad de luz en LUX del aula, se encienden o se apagan las luces.
 *Dependiendo el horario del día, se cierran o se abren las persianas.
 *
 * \section hardConn Hardware Connection
 *
 * | BTHC05			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		RX		| 		232TX	|
 * | 		TX	 	| 		232RX	|
 * | 		5V	 	| 		5V		|
 * | 		GND	 	| 		GND		|
 *
 * | LDR GL5528		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		OUT	 	| 		CH1		|
 * | 		3.3V	| 		3.3V	|
 * | 		GND	 	| 		GND		|
 *
 *
 * | SERVOMOTORSG90	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		PWM	 	| 	T_FIL2		|
 * | 		5V 		| 	5V			|
 * | 		GND	 	| 	GND			|