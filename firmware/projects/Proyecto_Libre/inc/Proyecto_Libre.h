/*! @mainpage Proyecto_Libre
 *
 * \section genDesc General Description
 *
 *Esta aplicación simula la automatizacion de luces y persianas en un aula.
 *Dependiendo de la cantidad de luz en LUX del aula, se encienden o se apagan las luces.
 *Dependiendo el horario del día, se cierran o se abren las persianas.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 *
 * | BTHC05			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		RX		| 		TX232	|
 * | 		TX	 	| 		RX232	|
 * | 		5V	 	| 		5V		|
 * | 		GND	 	| 		GND		|
 *
 * | LDR GL5528		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		OUT	 	| 		CH1		|
 * | 		3.3V	| 		3.3V	|
 * | 		GND	 	| 		GND		|
 *
 *
 * | SERVOMOTORSG90	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		PWM	 	| 	T_FIL2		|
 * | 		5V 		| 	5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 07/10/2021 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Acosta Azul
 *
 */

#ifndef _PROYECTO_LIBRE_H
#define _PROYECTO_LIBRE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTO_LIBRE_H */

