/*! @mainpage Proyecto_Libre
 *
 * \section genDesc General Description
 *
 * Esta aplicación simula la automatizacion de luces y persianas en un aula.
 *Dependiendo de la cantidad de luz en LUX del aula, se encienden o se apagan las luces.
 *Dependiendo el horario del día, se cierran o se abren las persianas.
 *
 * \section hardConn Hardware Connection
 *
 * | BTHC05			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		RX		| 		232TX	|
 * | 		TX	 	| 		232RX	|
 * | 		5V	 	| 		5V		|
 * | 		GND	 	| 		GND		|
 *
 * | LDR GL5528		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		OUT	 	| 		CH1		|
 * | 		3.3V	| 		3.3V	|
 * | 		GND	 	| 		GND		|
 *
 *
 * | SERVOMOTORSG90	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		PWM	 	| 	T_FIL2		|
 * | 		5V 		| 	5V			|
 * | 		GND	 	| 	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 07/10/2021 | Document creation		                         |
 * | 04/11/2021	| Documento finalizado						     |
 *
 * @author Azul Acosta
 * */

/*==================[inclusions]=============================================*/
#include "Proyecto_Libre.h"       /* <= own header */
#include "servomotor_sg90.h"
#include "sapi_rtc.h"
#include "led.h"
#include "BT_HC05.h"
#include "ldr_gl5528.h"
#include "timer.h"
#include "systemclock.h"
#include "gpio.h" /*Este driver no tiene que aparecer en esta capa pero al final utilice un LED externo
a la educiaa por lo que necesitaba un gpio*/

/*==================[macros and definitions]=================================*/
#define LUX_NORMAL 500 /*lux normal*/

/*==================[internal data definition]===============================*/

 uint8_t HORA_MINIMA=7;
 uint8_t HORA_MAXIMA=21;
 uint8_t Estado_Persiana;
 char modo;
 char automatico;
 char manual;
 uint32_t Cantidad_Lux;
 uint16_t iter=0;
 uint32_t acumulador;
 uint32_t Promedio_Lux_10seg;
 rtc_t Fecha_Hora={2021,11,12,5,15,30,10};
 const gpio_t pin_led= {GPIO_5};


/*==================[internal functions declaration]=========================*/
void AbrirPersiana(){
	ServoMover(0);
}

void CerrarPersiana(){
	ServoMover(90);

}

/*Interrupcion para UART*/
void DatosAppAPC(){

	BTLeerDatos(&Estado_Persiana);

	if(Estado_Persiana=='C'){
		 modo=automatico;
    		}
	if(Estado_Persiana=='D'){
		modo=manual;
	 AbrirPersiana();}

	if(Estado_Persiana=='d'){
			modo=manual;
			CerrarPersiana();}
}

/*Interrupcion para timer*/
void DatosPCAApp(){

	rtcRead(&Fecha_Hora);
	BTEnviarDatos(&Fecha_Hora,Cantidad_Lux);
}

timer_config Mi_Timer={TIMER_A,1000,&DatosPCAApp};

/*todas las inicializaciones en una sola funcion*/
void SistemaInit(){
	SystemClockInit();
	rtcConfig(&Fecha_Hora);
	BTInit(&DatosAppAPC);
	LDRInit();
	GPIOInit(GPIO_5,GPIO_OUTPUT);
	ServoInit();
	LedsInit();
	TimerInit(&Mi_Timer);
	TimerStart(TIMER_A);


}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

SistemaInit();

    while(1){
/*Persianas*/

    	if(modo=(automatico)){

    	if((Fecha_Hora.hour > HORA_MINIMA)&&(Fecha_Hora.hour < HORA_MAXIMA)){
    		AbrirPersiana();
    	}
    		else{
    			CerrarPersiana();
    		}
    	}

/*Luces*/
    
    DatoLux(&Cantidad_Lux);

   /*Procesamiento de la señal
    *  if(iter<10){
    	iter++;
    	acumulador+=Cantidad_Lux;

    }
    	else{
    		Promedio_Lux_10seg=acumulador/10;
    		iter=0;
    		acumulador=0; */

    if(Cantidad_Lux<LUX_NORMAL){
    				GPIOOn(GPIO_5);
    				LedOn(LED_1);

    			}
    				else{
    					GPIOOff(GPIO_5);
    					LedOff(LED_1);
    				}
    	}
    return 0;}

/*==================[end of file]============================================*/

