/*! @mainpage Contador_de_objetos_Cinta
 *
 * \section genDesc General Description
 *
 *Proyecto 2: Contador de objetos en cinta transportadora
 *Cuenta  objetos en una cinta transportadora mediante el sensor Tcrt 5000, utiliza el driver tcrt5000 y DisplayITS_E0803.
 *Muestra la cantidad de objetos contados utilizando los leds como un contador binario:
 *el LED_RGB_B (Azul) como el b3, el LED_1 como b2, el LED_2 como b1y el LED_3 como b0.
 *Muestra la cantidad de objetos contados utilizando el display LCD.
 *Se utilizan las teclas:TEC1 para activar y detener el conteo, TEC2 para mantener el resultado HOLD y TEC3 para resetear el conteo.
 *
 * \section hardConn Hardware Connection
 *
 * | ITS_E0803		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND 		| 	GND			|
 *
 * | TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	T_COL0		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND	 		| 	GND			|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/09/2021 | Documento creado	                        	 |
 * | 16/09/2021 | Documento terminado	                         |
 *
 * @author Acosta Azul y Ana Pabla Molina van Leeuwen
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/contador_de_objetos.h"       /* <= own header */

#include "systemclock.h"
#include "tcrt5000.h"
#include "led.h"
#include "delay.h"
#include "gpio.h"
#include "switch.h"
#include "bool.h"
#include "DisplayITS_E0803.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/
#define CANTIDAD_BITS 4 /*cantidad de bits*/
/*Banderas del estado pin sensor*/
bool estado_pin=false;
bool estado_anterior=false;
/*Banderas de las teclas*/
bool TEC1_flg=false;
bool TEC2_flg=false;
bool TEC3_flg=false;

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/
/*Funcion que pasa número a binario.
 *Digito es el valor que necesitamos pasar a binario, y guarda en vector_numero
 */
void Numero_A_Binario(uint8_t digito,uint8_t *vector_numero ){
	uint8_t i;

	for(i=CANTIDAD_BITS;i>0;i--){
		if (digito%2==1){
			vector_numero[i-1]=1;
		}
		else{
			vector_numero[i-1]=0;
		}
	digito=digito/2;
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	gpio_t pin_sensor=GPIO_T_COL0; /* pin de entrada al sensor*/
	Tcrt5000Init(pin_sensor); /*inicializo el pin como entrada*/

	gpio_t pin_leds[4]={GPIO_LED_3, GPIO_LED_2, GPIO_LED_1, GPIO_LED_RGB_B};
	LedsInit();

	gpio_t pin_display={GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	ITSE0803Init(&pin_display);

	uint8_t teclas;
	SwitchesInit();

	uint8_t contador= 0;/*variable para contar los objetos*/
	uint8_t contador_HOLD=0;/*variable para guardar el valor del contador*/
	uint8_t contador_mostrar=0;/*variable que guarda el valor a mostrar en los leds y display*/

    while(1){
    	teclas = SwitchesRead(); /*lee la tecla y la guarda*/
    	estado_pin=Tcrt5000State();/*lee el estado del pin*/

   //Teclas
    	    	switch(teclas)
    	    	    	{
    	    	    		case SWITCH_1:/*activa si TEC1_flg==TRUE y detiene el conteo si TEC1_flg==FALSE*/
    	    	    			TEC1_flg=!TEC1_flg;
    	    	    		break;
    	    	    		case SWITCH_2: /*mantener el resultado en HOLD*/
    	    	    			TEC2_flg=!TEC2_flg;
    	    	    			contador_HOLD=contador;
    	    	    		break;
    	    	    		case SWITCH_3: /*resetear el conteo*/
    	    	    			TEC3_flg=!TEC3_flg;
    	    	    		   contador=0;
    	    	    		break;
    	    	    	}

    	if(TEC1_flg){
    		/*Lee el estado actual y el anterior, entra al if cuando hay un cambio de estado pin*/
    						if((estado_pin==true)&(estado_anterior==false)){
										contador ++; /*cuenta objeto*/

    																		}
    				}
    	/*TEC1_flg en alto*/
    	if(TEC2_flg==false){
    				contador_mostrar=contador;
    						}
    	/*TEC2_flg en bajo*/
    			else {
    				contador_mostrar=contador_HOLD;
    				}
    	/*TEC2_flg en alto*/

    	int8_t vector[CANTIDAD_BITS]= {0};

    	Numero_A_Binario(contador_mostrar, vector);
    	uint8_t a=0;
    	/*Prende LEDS, contador binario*/
    	for(a=0;a<CANTIDAD_BITS;a++){
    		if(vector[a]==1){
    				GPIOOn(pin_leds[a]);
    						}
    			else{
    			GPIOOff(pin_leds[a]);
    			}
    	}
    	/*Muestra en valor de contador en display*/
    	ITSE0803DisplayValue(contador_mostrar);

    	estado_anterior=estado_pin;
        DelayMs(500);


    }

	return 0;
}

/*==================[end of file]============================================*/

