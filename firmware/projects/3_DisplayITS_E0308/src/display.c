/*! @mainpage 3_DisplayITS_E0308
 *
 * \section genDesc General Description
 *
 * Esta aplicación controla una interfaz de salida LCD de 3 dígitos.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | ITS_E0803		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND 		| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/08/2021 | Creación del documento	                     |
 * | 09/09/2021 | Documento terminado						     |
 *
 * @author Acosta Azul
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/display.h"       /* <= own header */

#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "delay.h"
#include "chip.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	gpio_t pin_es[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	uint16_t valor_ingresado=123;
	ITSE0803Init(pin_es);
	ITSE0803DisplayValue(valor_ingresado);
	ITSE0803ReadValue();

	return 0;
}

/*==================[end of file]============================================*/

