/*! @mainpage Contador_de_objetos_cinta_interrupciones_timer_uart
 *
 * \section genDesc General Description
 *
 *Proyecto 3:Contador de objetos en cinta transportadora
 *Diseñar y desarrollar un sistema embebido capaz de contar productos en una cinta transportadora.
 *El sistema deberá ser capaz de detectar y contar compartimentos, en los cuales se colocan los productos,
 *identificados con líneas negras y blancas empleando un detector de línea.
 *
 * \section hardConn Hardware Connection
 *
 * | ITS_E0803		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND 		| 	GND			|
 *
 * | TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	T_COL0		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND	 		| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * | 27/09/2021	| terminado                                      |
 *
 * @author Acosta Azul
 *
 */

#ifndef _5_CONTADOR_DE_OBJETOS_CINTA_UART_H
#define _5_CONTADOR_DE_OBJETOS_CINTA_UART_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _5_CONTADOR_DE_OBJETOS_CINTA_UART_H */

