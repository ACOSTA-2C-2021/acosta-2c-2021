/*! @mainpage Contador_de_objetos_cinta_interrupciones_timer_uart
 *
 * \section genDesc General Description
 *
 *Proyecto 3:Contador de objetos en cinta transportadora
 *Diseñar y desarrollar un sistema embebido capaz de contar productos en una cinta transportadora.
 *El sistema deberá ser capaz de detectar y contar compartimentos, en los cuales se colocan los productos,
 *identificados con líneas negras y blancas empleando un detector de línea.
 *
 * \section hardConn Hardware Connection
 *
 * | ITS_E0803		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND 		| 	GND			|
 *
 * | TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	T_COL0		|
 * | 	+5V	 		| 	+5V			|
 * | 	GND	 		| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2021 | Document creation		                         |
 * | 27/09/2021	| terminado                                      |
 *
 * @author Acosta Azul
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/5_CONTADOR_DE_OBJETOS_CINTA_UART.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "tcrt5000.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "bool.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define CANTIDAD_BITS 4 /*cantidad de bits*/

/*Banderas del estado del pin sensor*/
bool estado_pin=false;
bool estado_anterior=false;

/*Banderas de las teclas*/
bool TEC1_flg= false;
bool TEC2_flg= false;
bool TEC3_flg= false;

/*Bandera para timer*/
bool TIMER_1=false;
bool TIMER_2=false;

uint8_t vector[CANTIDAD_BITS]={0};/*vector con el numero binario*/
uint8_t contador= 0;/*variable para contar los objetos*/
uint8_t contador_HOLD= 0;/*variable para guardar el valor de contador*/
uint8_t contador_mostrar= 0;/*variable que guarda el valor a mostrar en los leds y display*/

/*Sensor:*/
gpio_t pin_sensor=GPIO_T_COL0;

/*Leds:*/
gpio_t pin_leds[4]={GPIO_LED_3,GPIO_LED_2,GPIO_LED_1,GPIO_LED_RGB_B};

/* Display:*/
gpio_t pin_Display[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};

/*Teclas:*/
uint8_t tecla;/*variable para guardar la tecla leida*/

/*Funcion que pasa número a binario.
 *Digito es el valor que necesitamos pasar a binario, y guarda en vector_numero
 */
void Numero_A_Binario(uint8_t digito,uint8_t *vector_numero ){
	uint8_t i;
	for(i=CANTIDAD_BITS;i>0;i--){
		if (digito%2==1){
			vector_numero[i-1]=1;
		}
			else{
			vector_numero[i-1]=0;
		}
	digito=digito/2;
	}
}

/*Funciones para interrupciones, generan el cambio de bandera*/
void Tecla1(){
	TEC1_flg =!TEC1_flg;
}

void Tecla2(){
		TEC2_flg =!TEC2_flg;
	}

void Tecla3(){
		TEC3_flg =!TEC3_flg;
}

/*Funcion para timer*/
void contador_cinta(){
			tecla = SwitchesRead();/*lee la tecla y se guarda*/
	    	estado_pin=Tcrt5000State();/*lee el estado del pin*/

	    	if (TEC1_flg){
	    		/*Lee el estado actual y anterior del pin, entra al if cuando hay un cambio*/
				if((estado_pin==true)&(estado_anterior==false)){
						contador ++;/*cuenta un objeto*/
				}
	    	}/*TEC1 esta en alto*/

	    	if (TEC2_flg == false){
	    		contador_mostrar=contador;
	    	}/*TEC2 esta en bajo*/
	    		else {
	    		contador_mostrar=contador_HOLD;
	    	}/*TEC2 esta en alto*/

	    	if (TEC3_flg){
	    		contador=0;
	    	}

Numero_A_Binario(contador_mostrar,vector);/* vector con el valor binario de contador_mostrar*/

/*Prende los leds según un contador de numero binario, se utiliza GPIO*/
			uint8_t j;
			for(j=0;j<CANTIDAD_BITS;j++){
				if(vector[j]==1) {
					GPIOOn(pin_leds[j]);
				}
					else {
					GPIOOff(pin_leds[j]);
				}
			}
/*Muestra en display el valor del contador*/
ITSE0803DisplayValue(contador_mostrar);

estado_anterior=estado_pin;

	    }

/*Timer Struct
*campos del struct, timer seleccionado, periodo y  puntero a funcion que se llamara repetitivamente*/
timer_config my_timer = {TIMER_A,1000,&contador_cinta};

uint8_t cantidad_lineas;

/*Funcion para uart*/
void Datos_A_PC(){

	UartReadByte(SERIAL_PORT_PC, &cantidad_lineas); /*Lee el estado del puerto que se le pase como parametro*/

							case 'o':
								Tecla1();
    	    	    		break;
    	    	    		case 'h':
    	    	    			Tecla2();
    	    	    		break;
    	    	    		case '0':
    	    	    			Tecla3();

    	    	    		break;
}

/*Serial Ports Struct
*campos puerto, velocidad en baudios, puntero de funcion al puerto serie de la aplicacion*/
serial_config UART_USB = {SERIAL_PORT_PC,115200, &Datos_A_PC};


/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void){

/*Funciones de inicializacion*/

	SystemClockInit();
	Tcrt5000Init(pin_sensor);
	LedsInit();
	ITSE0803Init(pin_Display);
	SwitchesInit();

/*Interrupciones*/

	SwitchActivInt(SWITCH_1 ,&Tecla1);
	SwitchActivInt(SWITCH_2 ,&Tecla2);
	SwitchActivInt(SWITCH_3 ,&Tecla3);

TimerInit(&my_timer); /*Puntero a estructura de inicializacion del timer*/

UartInit(&UART_USB); /*Inicializa UART*/
cantidad_lineas=UartItoa(contador_mostrar,10);
/*Conversor de entero a ASCII, contador_mostrar es el valor que se desea convertir a ASCII,
 * 10 es la base sobre la cual se desea realizar la conversion
 * Retorna un puntero al primer elemento de la cadena convertida (cantidad de lineas)*/

UartSendString(SERIAL_PORT_PC,&cantidad_lineas); /*manda la cadena de caracteres desde la placa a la pc*/
UartSendString(SERIAL_PORT_PC,"lineas \n\r");

	return 0;
}

/*==================[end of file]============================================*/

