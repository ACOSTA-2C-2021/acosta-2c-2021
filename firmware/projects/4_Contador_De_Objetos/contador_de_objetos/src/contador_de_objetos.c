/*! @mainpage Contador_de_objetos
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/contador_de_objetos.h"       /* <= own header */
#include "systemclock.h"
#include "tcrt5000.h"
#include "led.h"
#include "delay.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/
bool estado_pin=false;
bool estado_anterior=false;
/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	gpio_t pin=GPIO_T_COL0; // pin de entrada al sensor
	Tcrt5000Init(pin);// inicializo el pin como entrada

	SystemClockInit();
	LedsInit();
	uint16_t contador= 0;//variable para contar los objetos

    while(1){

    	estado_pin=Tcrt5000State();//lee el estado del pin

    	if((estado_pin==true)&(estado_anterior==false)){
				contador ++;
				LedOn(LED_2);// se prende el led 2,mientras cuente al objeto
    	}

    	else {
				LedOff(LED_2);
		}
    	estado_anterior=estado_pin;
    	DelayMs(500);
    }

	return 0;
}

/*==================[end of file]============================================*/

