var indexSectionsWithContent =
{
  0: "_abcdfghilmprstu",
  1: "admrst",
  2: "g",
  3: "abdgilmprstu",
  4: "bghimpt",
  5: "glps",
  6: "cg",
  7: "abcdgilmpstu",
  8: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Modules",
  8: "Pages"
};

