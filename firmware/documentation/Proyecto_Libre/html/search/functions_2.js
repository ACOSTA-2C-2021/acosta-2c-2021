var searchData=
[
  ['datolux',['DatoLux',['../group___l_d_r___g_l5528.html#ga8d9139ce2391cf5b8121297bc84b456b',1,'DatoLux(uint32_t *ilum_lux):&#160;LDR_GL5528.c'],['../group___l_d_r___g_l5528.html#ga8d9139ce2391cf5b8121297bc84b456b',1,'DatoLux(uint32_t *ilum_lux):&#160;LDR_GL5528.c']]],
  ['delayms',['DelayMs',['../group___delay.html#ga57655b974339443ccc09a5579b8dbeb8',1,'DelayMs(uint32_t msec):&#160;delay.c'],['../group___delay.html#ga57655b974339443ccc09a5579b8dbeb8',1,'DelayMs(uint32_t msec):&#160;delay.c']]],
  ['delaysec',['DelaySec',['../group___delay.html#ga07471e33d1ced89d722035841f872545',1,'DelaySec(uint32_t sec):&#160;delay.c'],['../group___delay.html#ga07471e33d1ced89d722035841f872545',1,'DelaySec(uint32_t sec):&#160;delay.c']]],
  ['delayus',['DelayUs',['../group___delay.html#gaaa99df3e6eae48f285374e576d9bb345',1,'DelayUs(uint32_t usec):&#160;delay.c'],['../group___delay.html#gaaa99df3e6eae48f285374e576d9bb345',1,'DelayUs(uint32_t usec):&#160;delay.c']]]
];
