var searchData=
[
  ['proyecto_5flibre',['Proyecto_Libre',['../index.html',1,'']]],
  ['panaloginput',['pAnalogInput',['../structanalog__input__config.html#a247d18bf22d03f3d67a47647bbf1d1ee',1,'analog_input_config']]],
  ['period',['period',['../structtimer__config.html#abd839b0572ca4c62c0e6137bf6fbe4a1',1,'timer_config']]],
  ['pfunc',['pFunc',['../structtimer__config.html#afaafe064d9f374c518774856f9fd1726',1,'timer_config']]],
  ['port',['port',['../structserial__config.html#a5430eba070493ffcc24c680a8cce6b55',1,'serial_config']]],
  ['pserial',['pSerial',['../structserial__config.html#a3f4ce60ef262396e928d64249813b659',1,'serial_config']]],
  ['ptr_5fgpio_5fgroup_5fint_5ffunc',['ptr_GPIO_group_int_func',['../group___g_p_i_o.html#gadb1b43449a7ec81462b1e8ae68041b50',1,'gpio.c']]],
  ['ptr_5fgpio_5fint_5ffunc',['ptr_GPIO_int_func',['../group___g_p_i_o.html#gac7d9672849de0a3c41c38280af236661',1,'gpio.c']]],
  ['pwm_5fout_5ft',['pwm_out_t',['../group___p_w_m__sct.html#gaf28a35f298e221200e47ba03ed074eee',1,'pwm_sct.h']]],
  ['pwm_20sct',['PWM sct',['../group___p_w_m__sct.html',1,'']]],
  ['pwmdeinit',['PWMDeinit',['../group___p_w_m__sct.html#gaf0bd834d21081a27e8c0d0df14e558e2',1,'PWMDeinit(void):&#160;pwm_sct.c'],['../group___p_w_m__sct.html#gaf0bd834d21081a27e8c0d0df14e558e2',1,'PWMDeinit(void):&#160;pwm_sct.c']]],
  ['pwminit',['PWMInit',['../group___p_w_m__sct.html#gaaa8f960523fc95ecfcc422e5f5843e01',1,'PWMInit(pwm_out_t *ctout, uint8_t n_outs, uint16_t freq):&#160;pwm_sct.c'],['../group___p_w_m__sct.html#gaaa8f960523fc95ecfcc422e5f5843e01',1,'PWMInit(pwm_out_t *ctout, uint8_t n_outs, uint16_t freq):&#160;pwm_sct.c']]],
  ['pwmoff',['PWMOff',['../group___p_w_m__sct.html#ga22c7eecc5b183661ce6a5649f455d45f',1,'PWMOff(void):&#160;pwm_sct.c'],['../group___p_w_m__sct.html#ga22c7eecc5b183661ce6a5649f455d45f',1,'PWMOff(void):&#160;pwm_sct.c']]],
  ['pwmon',['PWMOn',['../group___p_w_m__sct.html#ga90a0a0967bf7e8f0ed14da97988c6d71',1,'PWMOn(void):&#160;pwm_sct.c'],['../group___p_w_m__sct.html#ga90a0a0967bf7e8f0ed14da97988c6d71',1,'PWMOn(void):&#160;pwm_sct.c']]],
  ['pwmsetdutycycle',['PWMSetDutyCycle',['../group___p_w_m__sct.html#ga2df6527368e723f864d17549bfd04997',1,'PWMSetDutyCycle(pwm_out_t ctout, uint8_t duty_cycle):&#160;pwm_sct.c'],['../group___p_w_m__sct.html#ga2df6527368e723f864d17549bfd04997',1,'PWMSetDutyCycle(pwm_out_t ctout, uint8_t duty_cycle):&#160;pwm_sct.c']]],
  ['pwmsetfreq',['PWMSetFreq',['../group___p_w_m__sct.html#ga39385387144d35a7b664080b29e2a873',1,'PWMSetFreq(uint32_t freq):&#160;pwm_sct.c'],['../group___p_w_m__sct.html#ga39385387144d35a7b664080b29e2a873',1,'PWMSetFreq(uint32_t freq):&#160;pwm_sct.c']]]
];
