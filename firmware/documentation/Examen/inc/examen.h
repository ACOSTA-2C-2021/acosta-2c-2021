/*! @mainpage Examen
 * \section genDesc General Description
 *
 * \section genDesc General Description
 *
 *Se pretende diseñar un dispositivo que se utilizará como detector de objetos cercanos (sonar) en un robot móvil.
 *Dicho dispositivo cuenta con un HC-SR04 para el sensado de los objetos y es ubicado en distintas
 *orientaciones por medio de un motor.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | HC_SR4			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	5V			|
 * | 	GND		 	| 	GND			|
 * | 	ECHO	 	| 	GPIO_T_FIL2	|
 * | 	TRIGGER		| 	GPIO_T_FIL0	|
 *
 * | POTENCIOMETRO	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	3.3V		|
 * | 	GND		 	| 	GND			|
 * | 	OUT		 	| 	CH1			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * | 01/11/2021	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Azul Acosta
 *
 */

#ifndef _EXAMEN_H
#define _EXAMEN_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EXAMEN_H */

