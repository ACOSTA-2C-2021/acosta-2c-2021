var NAVTREEINDEX1 =
{
"group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c":[3,0,0,10,3],
"group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9":[3,0,0,10,2],
"group___switch.html#gaa87203a5637fb4759a378b579aaebff6":[3,0,0,10,0],
"group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262":[3,0,0,10,4],
"group___systemclock.html":[3,0,1,2],
"group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5":[3,0,1,2,0],
"group___t_c_r_t5000.html":[3,0,0,11],
"group___t_c_r_t5000.html#ga51671fa99fcdfd5bfb58feb05fca49b9":[3,0,0,11,2],
"group___t_c_r_t5000.html#gab4bfab14f44be848c953f96363908f96":[3,0,0,11,1],
"group___t_c_r_t5000.html#gaf64bb75b4a2cea914208f99c462abeeb":[3,0,0,11,0],
"group___u_a_r_t.html":[3,0,1,3],
"group___u_a_r_t.html#ga18acc2b11b5c032105e7c2d6667d653f":[3,0,1,3,3],
"group___u_a_r_t.html#ga9ae7a88227b684e39006f69642a1e90d":[3,0,1,3,1],
"group___u_a_r_t.html#gad381bf998d964c17bfb5818cd3b39464":[3,0,1,3,2],
"group__goniometro.html":[3,0,0,4],
"group__goniometro.html#ga1867480ba4b7e4e87267e25d0a3506b7":[3,0,0,4,0],
"group__goniometro.html#ga6e949327dfca00992d8905815d54ebe3":[3,0,0,4,1],
"hc__sr4_8c_source.html":[5,0,0,0,0,1,4],
"hc__sr4_8h_source.html":[5,0,0,0,0,0,5],
"i2c_8c_source.html":[5,0,0,0,1,1,2],
"i2c_8h_source.html":[5,0,0,0,1,0,3],
"index.html":[],
"index.html#changelog":[2],
"index.html#genDesc":[0],
"index.html#hardConn":[1],
"led_8c_source.html":[5,0,0,0,0,1,6],
"led_8h_source.html":[5,0,0,0,0,0,7],
"modules.html":[3],
"pages.html":[],
"pwm__sct_8c_source.html":[5,0,0,0,0,1,9],
"pwm__sct_8h_source.html":[5,0,0,0,0,0,10],
"sapi__rtc_8c_source.html":[5,0,0,0,0,1,10],
"sapi__rtc_8h_source.html":[5,0,0,0,0,0,11],
"structanalog__input__config.html":[3,0,0,12,0],
"structanalog__input__config.html#a247d18bf22d03f3d67a47647bbf1d1ee":[3,0,0,12,0,2],
"structanalog__input__config.html#a4281217279705a0cd501489d13610401":[3,0,0,12,0,1],
"structanalog__input__config.html#a55dca9964f1ce6bd422c3ac977196b11":[3,0,0,12,0,0],
"structdigital_i_o.html":[3,2,0],
"structdigital_i_o.html#a193f5c6819b21ca6b603d25f05b040ad":[3,2,0,1],
"structdigital_i_o.html#a51f75cbcf4e53d4a37a00155a316c38f":[3,2,0,4],
"structdigital_i_o.html#a79691c4619ba92dbf4859aaa6e006531":[3,2,0,3],
"structdigital_i_o.html#a93d2e4a48daa464205632175fdb7288c":[3,2,0,2],
"structdigital_i_o.html#aeaf3b63ddbf55482742c6be93f702a03":[3,2,0,0],
"structmma8451__config__t.html":[3,0,0,8,0],
"structmma8451__config__t.html#a6dc50f1cf7d27b4418c2155a6ea08b0e":[3,0,0,8,0,0],
"structmma8451__config__t.html#a8c410734c6329c3353d351881ae27b1b":[3,0,0,8,0,1],
"structrtc__t.html":[4,0,3],
"structrtc__t.html#a006ff53c1e8cc7384bca0518e78b50cf":[4,0,3,5],
"structrtc__t.html#a0b1179ee69a6ebaafdef44da8f0e03f8":[4,0,3,6],
"structrtc__t.html#a69fc0d8fa76da1a04e0695e101ee4e56":[4,0,3,3],
"structrtc__t.html#a786e66fcc38100b5dd231093318df2b1":[4,0,3,4],
"structrtc__t.html#aae4f8ffdc637393d5aa73fafb4688b48":[4,0,3,1],
"structrtc__t.html#ab3b2edf5633eb34fafd79918b68d44ba":[4,0,3,0],
"structrtc__t.html#aee6b9af2160384b4dae9355c99ae8bb5":[4,0,3,2],
"structserial__config.html":[3,0,1,3,0],
"structserial__config.html#a3f4ce60ef262396e928d64249813b659":[3,0,1,3,0,2],
"structserial__config.html#a5430eba070493ffcc24c680a8cce6b55":[3,0,1,3,0,1],
"structserial__config.html#ae28e9ec22e61f2fd869555513c0857c0":[3,0,1,3,0,0],
"structtimer__config.html":[3,1,0,0,0],
"structtimer__config.html#ab42a22a518af439f16e4d09e51aa2553":[3,1,0,0,0,2],
"structtimer__config.html#abd839b0572ca4c62c0e6137bf6fbe4a1":[3,1,0,0,0,0],
"structtimer__config.html#afaafe064d9f374c518774856f9fd1726":[3,1,0,0,0,1],
"switch_8c_source.html":[5,0,0,0,0,1,12],
"switch_8h_source.html":[5,0,0,0,0,0,13],
"systemclock_8c_source.html":[5,0,0,0,1,1,3],
"systemclock_8h_source.html":[5,0,0,0,1,0,4],
"timer_8c_source.html":[5,0,0,0,1,1,4],
"timer_8h_source.html":[5,0,0,0,1,0,5],
"uart_8c_source.html":[5,0,0,0,1,1,5],
"uart_8h_source.html":[5,0,0,0,1,0,6]
};
