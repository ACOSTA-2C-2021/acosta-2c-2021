var searchData=
[
  ['datoangulo',['DatoAngulo',['../group__goniometro.html#ga1867480ba4b7e4e87267e25d0a3506b7',1,'DatoAngulo(uint16_t *angulo):&#160;Goniometro.c'],['../group__goniometro.html#ga1867480ba4b7e4e87267e25d0a3506b7',1,'DatoAngulo(uint16_t *angulo):&#160;Goniometro.c']]],
  ['datolux',['DatoLux',['../group___l_d_r___g_l5528.html#ga91ec9ad2f2d8e9d2d880addecfbdc145',1,'DatoLux(uint16_t *ilum_lux):&#160;LDR_GL5528.c'],['../group___l_d_r___g_l5528.html#ga91ec9ad2f2d8e9d2d880addecfbdc145',1,'DatoLux(uint16_t *ilum_lux):&#160;LDR_GL5528.c']]],
  ['delay',['Delay',['../group___delay.html',1,'']]],
  ['delayms',['DelayMs',['../group___delay.html#ga57655b974339443ccc09a5579b8dbeb8',1,'DelayMs(uint32_t msec):&#160;delay.c'],['../group___delay.html#ga57655b974339443ccc09a5579b8dbeb8',1,'DelayMs(uint32_t msec):&#160;delay.c']]],
  ['delaysec',['DelaySec',['../group___delay.html#ga07471e33d1ced89d722035841f872545',1,'DelaySec(uint32_t sec):&#160;delay.c'],['../group___delay.html#ga07471e33d1ced89d722035841f872545',1,'DelaySec(uint32_t sec):&#160;delay.c']]],
  ['delayus',['DelayUs',['../group___delay.html#gaaa99df3e6eae48f285374e576d9bb345',1,'DelayUs(uint32_t usec):&#160;delay.c'],['../group___delay.html#gaaa99df3e6eae48f285374e576d9bb345',1,'DelayUs(uint32_t usec):&#160;delay.c']]],
  ['digitalio',['digitalIO',['../structdigital_i_o.html',1,'']]],
  ['display',['Display',['../group___display.html',1,'']]],
  ['drivers_20devices',['Drivers devices',['../group___drivers___devices.html',1,'']]],
  ['drivers_20microcontroller',['Drivers microcontroller',['../group___drivers___microcontroller.html',1,'']]],
  ['drivers_20programable',['Drivers Programable',['../group___drivers___programable.html',1,'']]]
];
