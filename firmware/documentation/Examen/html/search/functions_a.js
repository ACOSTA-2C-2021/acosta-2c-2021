var searchData=
[
  ['tcrt5000deinit',['Tcrt5000Deinit',['../group___t_c_r_t5000.html#gaf64bb75b4a2cea914208f99c462abeeb',1,'Tcrt5000Deinit(gpio_t dout):&#160;Tcrt5000.c'],['../group___t_c_r_t5000.html#gaf64bb75b4a2cea914208f99c462abeeb',1,'Tcrt5000Deinit(gpio_t dout):&#160;Tcrt5000.c']]],
  ['tcrt5000init',['Tcrt5000Init',['../group___t_c_r_t5000.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;Tcrt5000.c'],['../group___t_c_r_t5000.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;Tcrt5000.c']]],
  ['tcrt5000state',['Tcrt5000State',['../group___t_c_r_t5000.html#ga51671fa99fcdfd5bfb58feb05fca49b9',1,'Tcrt5000State(void):&#160;Tcrt5000.c'],['../group___t_c_r_t5000.html#ga51671fa99fcdfd5bfb58feb05fca49b9',1,'Tcrt5000State(void):&#160;Tcrt5000.c']]],
  ['timerinit',['TimerInit',['../group___baremetal.html#ga148b01475111265d1798f5c204a93df0',1,'timer.c']]],
  ['timerreset',['TimerReset',['../group___baremetal.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'timer.c']]],
  ['timerstart',['TimerStart',['../group___baremetal.html#ga31487bffd934ce838a72f095f9231b24',1,'timer.c']]],
  ['timerstop',['TimerStop',['../group___baremetal.html#gab652b899be3054eae4649a9063ec904b',1,'timer.c']]]
];
