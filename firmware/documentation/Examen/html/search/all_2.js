var searchData=
[
  ['bare_20metal_20example_20header_20file',['Bare Metal example header file',['../group___baremetal.html',1,'']]],
  ['baud_5frate',['baud_rate',['../structserial__config.html#ae28e9ec22e61f2fd869555513c0857c0',1,'serial_config']]],
  ['bool',['bool',['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html',1,'(Global Namespace)']]],
  ['bt_5fhc05',['BT_HC05',['../group___b_t___h_c05.html',1,'']]],
  ['btenviardatos',['BTEnviarDatos',['../group___b_t___h_c05.html#gaeafb528685edbe163ce462413c4e4e1d',1,'BT_HC05.h']]],
  ['btinit',['BTInit',['../group___b_t___h_c05.html#ga91c0b12ea202490b614a9ce38f38cfb4',1,'BTInit(void *funcion):&#160;BT_HC05.c'],['../group___b_t___h_c05.html#ga91c0b12ea202490b614a9ce38f38cfb4',1,'BTInit(void *funcion):&#160;BT_HC05.c']]],
  ['btleerdatos',['BTLeerDatos',['../group___b_t___h_c05.html#gafb6e85f52cee8aef12ec8c921694cdf3',1,'BTLeerDatos(uint8_t Estado):&#160;BT_HC05.c'],['../group___b_t___h_c05.html#gafb6e85f52cee8aef12ec8c921694cdf3',1,'BTLeerDatos(uint8_t Estado):&#160;BT_HC05.c']]]
];
