var searchData=
[
  ['serial_5fconfig',['serial_config',['../structserial__config.html',1,'']]],
  ['servoinit',['ServoInit',['../group___s_e_r_v_o_m_o_t_o_r___s_g90.html#ga00f0bfb5a297c4896d5a95c514bc2b7a',1,'ServoInit():&#160;Servomotor_SG90.c'],['../group___s_e_r_v_o_m_o_t_o_r___s_g90.html#ga00f0bfb5a297c4896d5a95c514bc2b7a',1,'ServoInit():&#160;Servomotor_SG90.c']]],
  ['servomotor_5fsg90',['SERVOMOTOR_SG90',['../group___s_e_r_v_o_m_o_t_o_r___s_g90.html',1,'']]],
  ['servomover',['ServoMover',['../group___s_e_r_v_o_m_o_t_o_r___s_g90.html#gac6877dc1a157f6871e389521d30f2387',1,'ServoMover(uint16_t grados):&#160;Servomotor_SG90.c'],['../group___s_e_r_v_o_m_o_t_o_r___s_g90.html#gac6877dc1a157f6871e389521d30f2387',1,'ServoMover(uint16_t grados):&#160;Servomotor_SG90.c']]],
  ['switch',['Switch',['../group___switch.html',1,'']]],
  ['switchactivint',['SwitchActivInt',['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t tec, void *ptrIntFunc):&#160;switch.c'],['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t sw, void *ptr_int_func):&#160;switch.c']]],
  ['switches',['SWITCHES',['../group___switch.html#gaa87203a5637fb4759a378b579aaebff6',1,'switch.h']]],
  ['switchesactivgroupint',['SwitchesActivGroupInt',['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t tecs, void *ptrIntFunc):&#160;switch.c'],['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t switchs, void *ptr_int_func):&#160;switch.c']]],
  ['switchesinit',['SwitchesInit',['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c'],['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c']]],
  ['switchesread',['SwitchesRead',['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c'],['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c']]],
  ['systemclock',['Systemclock',['../group___systemclock.html',1,'']]],
  ['systemclockinit',['SystemClockInit',['../group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5',1,'SystemClockInit(void):&#160;systemclock.c'],['../group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5',1,'SystemClockInit(void):&#160;systemclock.c']]]
];
