var annotated_dup =
[
    [ "analog_input_config", "structanalog__input__config.html", "structanalog__input__config" ],
    [ "digitalIO", "structdigital_i_o.html", "structdigital_i_o" ],
    [ "mma8451_config_t", "structmma8451__config__t.html", "structmma8451__config__t" ],
    [ "rtc_t", "structrtc__t.html", "structrtc__t" ],
    [ "serial_config", "structserial__config.html", "structserial__config" ],
    [ "timer_config", "structtimer__config.html", "structtimer__config" ]
];