/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup SERVOMOTOR_SG90
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Azul Acosta
 *
 * This driver provide funciones para incializar pines GPIO, mostrar valor ingresado por el usuario en display, leer valor de display
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

#ifndef LDR_H_
#define LDR_H_

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/




#endif /* LDR_H_ */




