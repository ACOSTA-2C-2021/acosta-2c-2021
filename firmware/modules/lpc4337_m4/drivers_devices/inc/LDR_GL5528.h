/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup LDR_GL5528
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Azul Acosta
 *
 * Driver para LDR_GL5528 se utiliza conectada en serie con una resistencia de calibracion de 47Kohm, con una alimentacion de 5V.
 * Este driver inicializa el ADC,y convierte en valor digital de salida del ADC en LUX.
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/10/2021 | Document creation		                         |
 * | 04/11/2021	| Documento finalizado						     |
 */

#ifndef LDR_GL5528_H_
#define LDR_GL5528_H_

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"
#include "analog_io.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/**
 * @brief Inicializa el ADC, lee el dato de entrada analogica, lo convierte en digital y
 *  lo guarda en variable luz_aux_dig
 * @param[in] nada
 * @return TRUE
 */
bool LDRInit();

/**
 * @brief El dato digital guardado en luz_aux_dig lo convierte en LUX
 * @param[in] puntero a un entero de 32 bits sin signo ilum_lux
 * @return TRUE
 */
bool DatoLux(uint32_t *ilum_lux);


#endif /* LDR_GL5528_H_ */




