/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Display
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Azul Acosta
 *
 * This driver provide funciones para incializar pines GPIO, mostrar valor ingresado por el usuario en display, leer valor de display
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/09/2021 | Documento finalizado	                         |
 *
 */

#ifndef DISPLAYITS_E0803_H_
#define DISPLAYITS_E0803_H_

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/** @fn bool ITSE0803Init(gpio_t * pins)
 * @brief Función que inicializa pines
 * @param[in] pins lista de pines donde se conecta el display
 * @return  TRUE
 */
bool ITSE0803Init(gpio_t * pins);

/** @fn bool ITSE0803DisplayValue(uint16_t valor)
 * @brief Función que muestra el valor ingresado en el display
 * @param[in] valor entero que ingresa el usuario
 * @return  TRUE
 */
bool ITSE0803DisplayValue(uint16_t valor);

/** @fn uint16_t ITSE0803ReadValue(void)
 * @brief Función que lee el valor ingresado en el display
 * @param[in]
 * @return  entero
 */
uint16_t ITSE0803ReadValue(void);

/** @fn bool ITSE0803Deinit(gpio_t * pins)
 * @brief Función que no hace nada
 * @param[in]
 * @return  TRUE
 */
bool ITSE0803Deinit(gpio_t * pins);


#endif /* DISPLAYITS_E0803_H_ */




