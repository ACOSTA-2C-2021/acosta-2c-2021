/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup BT_HC05
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Azul Acosta
 *
 * Este driver es para bluetooth HC05. Inicializa uart, envia y lee datos.
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/10/2021 | Document creation		                         |
 * | 04/11/2021	| Documento finalizado						     |
 */

#ifndef BT_HC05_H_
#define BT_HC05_H_

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"
#include "uart.h"
#include "sapi_rtc.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/**
 * @brief Inicializa la uart
 * @param[in] Puntero a la funcion de interrupcion de la uart
 * @return TRUE
 */
bool BTInit(void *funcion);

/**
 * @brief Envia fecha, hora y cantidad de lux mediante Bluetooth a app del celular.
 * @param[in] Puntero a un tipo de dato rtc_t TIEMPO y entero sin signo de 32 bits LUX
 * @return TRUE
 */
bool BTEnviarDatos(rtc_t *Tiempo, uint32_t Lux);

/**
 * @brief Lee dato mediante bluetooth
 * @param[in] entero sin signo de 8 bits Estado
 * @return TRUE
 */
bool BTLeerDatos(uint8_t *Estado);


#endif /* BT_HC05_H_ */




