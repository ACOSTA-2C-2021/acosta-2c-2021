/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup goniometro
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Azul Acosta
 *
 *  Driver para goniometro, inicializa el CAD y dependiendo el valor de tension digital
 * devuelve un valor de angulo
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021 	| Document creation		                         |
 *
 */

#ifndef GONIOMETRO_H_
#define GONIOMETRO_H_

/*==================[inclusions]=============================================*/
#include"goniometro.h"
#include "analog_io.h"
#include "bool.h"


/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/**
 * @brief Inicializa el ADC, lee el dato de entrada analogica, lo convierte en digital y
 *  lo guarda en variable vtension
 * @param[in]
 * @return
 */
void GoniometroInit();

/**
 * @brief El dato digital guardado en vtension lo convierte en grados
 * @param[in] puntero a un entero de 16 bits sin signo angulo
 * @return TRUE
 */
bool DatoAngulo(uint16_t *angulo);


#endif /* LDR_GL5528_H_ */


