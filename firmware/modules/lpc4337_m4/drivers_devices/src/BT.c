/*! @mainpage LDR
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Servo SG90		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PWM		 	| 	TFIL_2		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 07/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Acosta Azul
 *
 */
/*==================[inclusions]=============================================*/

#include "gpio.h"
#include "chip.h"
#include "uart.h"
#include "sapi_rtc.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


bool BTInit(void *funcion){
	serial_config App_BT={SERIAL_PORT_P2_CONNECTOR, 115200, &funcion};
	UartInit(&App_BT);

return true;
}


bool BTEnviarDatos(rtc_t *Tiempo, uint16_t Lux){

	UartSendString(SERIAL_PORT_P2_CONNECTOR, Tiempo->year);
	UartSendString(SERIAL_PORT_P2_CONNECTOR, Tiempo->month);
	UartSendString(SERIAL_PORT_P2_CONNECTOR, Tiempo->mday);
	UartSendString(SERIAL_PORT_P2_CONNECTOR, Tiempo->wday);
	UartSendString(SERIAL_PORT_P2_CONNECTOR, Tiempo->hour);
	UartSendString(SERIAL_PORT_P2_CONNECTOR, Tiempo->min);
	UartSendString(SERIAL_PORT_P2_CONNECTOR, Tiempo->sec);
	//me falta enviar la cantidad de lux
	UartSendByte(SERIAL_PORT_P2_CONNECTOR,Lux);

bool BTLeerDatos(uint8_t Estado){

	UartReadByte(SERIAL_PORT_P2_CONNECTOR, &Estado);

}

return true;
}




/*==================[end of file]============================================*/


