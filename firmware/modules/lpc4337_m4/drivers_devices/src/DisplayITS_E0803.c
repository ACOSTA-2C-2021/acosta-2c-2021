/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Display
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Azul Acosta
 *
 * This driver provide funciones para incializar pines GPIO, mostrar valor ingresado por el usuario en display, leer valor de display
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/09/2021 | Documento finalizado	                         |
 *
 */
/*==================[inclusions]=============================================*/
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "delay.h"
#include "chip.h"

/*==================[macros and definitions]=================================*/
/*cantidad de GPIO necesarios para configurar el LCD (7GPIO para conectar los 3 BCD, todos de salida)*/
#define CANTIDAD_GPIO 7
/*cantidad de digitos que muestra el DISPLAY*/
#define CANTIDAD_DIGITOS 3
/*cantidad de byte*/
#define CANTIDAD_BYTE 4
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/* funcion que recibe un dato de 16 bit, lo separa en cada uno de sus bits y los convierte a Bcd,
* devolviendolos a traves de la direccion de memoria del puntero bcd_number */
void Binario_A_Bcd(uint16_t data, uint8_t digits, uint8_t *bcd_number);

/* función que toma un dígito BCD y lo separa en cada uno de sus bit y los guarda en los datos de D1 a D4 */
void Digito_A_Bit(uint8_t bcd);


/*==================[internal data definition]===============================*/
gpio_t pines[7];

/*variable que guarda el dato ingresado*/
uint16_t valor_numerico=0;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
void Binario_A_Bcd(uint16_t data, uint8_t digits, uint8_t *bcd_number){
	uint8_t i=0;
	while(i<digits){
	bcd_number[i]=data%10;
	data=data/10;
	i++;
	}
}

void Digito_A_Bit(uint8_t bcd){
	uint8_t i,aux; //se va  a guardar en pines[0..4], la configuracion si esta en alto o en bajo
	for(i=0;i<CANTIDAD_BYTE;i++){
			aux=bcd&1;
			if(aux==0){
				GPIOOff(pines[i]);
			}
			else{
				GPIOOn(pines[i]);}
			bcd=(bcd>>1);
	}
}



/*==================[external functions definition]==========================*/
/*función de inicializacion, para no recibir 7GPIO le paso un vector de GPIO, todos de salida*/
bool ITSE0803Init(gpio_t * pins){
	uint8_t i;
	for(i=0; i<CANTIDAD_GPIO;i++){
		pines[i]=pins[i];
		GPIOInit(pins[i],GPIO_OUTPUT);
	}
return true;
}

bool ITSE0803DisplayValue(uint16_t valor){
	valor_numerico=valor; /*guardo valor para usarlo en readvalue*/
	uint8_t datos[3];
	Binario_A_Bcd(valor,CANTIDAD_DIGITOS,datos); /*de Binario a BCD me devuelve un vector*/
	/*si el numero es 123, me devuelve [3,2,1]*/
	uint8_t i;
	for(i=0; i<CANTIDAD_DIGITOS;i++){
		Digito_A_Bit(datos[i]); /*Toma el BCD y separa en cada uno de sus bits, me guarda configuracion de D1 a D4*/
		GPIOOn(pines[6-i]); /*si i es 0 esta en SEL3, si i es 1 SEL2, si i es 2 SEL1*/
		GPIOOff(pines[6-i]);
	}
	return true;
}

uint16_t ITSE0803ReadValue(void){
	return valor_numerico;

}

bool ITSE0803Deinit(gpio_t * pins){
	void GPIODeinit();
	return true;
}


/*==================[end of file]============================================*/


