/* Copyright 2016,
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version Leando Medus
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 * 20210916 v1.2 modification made by Maximiliano Cantarutti. (proyect 2)
 */

/*==================[inclusions]=============================================*/
#include "switch.h"
#include "gpio.h"
#include "delay.h"


/*==================[macros and definitions]=================================*/

gpio_t echo_g;
gpio_t trigger_g;
#define TRIGGER 10
#define ECHO 1

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger);
int16_t HcSr04ReadDistance (void);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	GPIOInit(echo, GPIO_INPUT);/*entrada */
	GPIOInit(trigger, GPIO_OUTPUT);/*salida */
	GPIOOff(trigger);
	echo_g = echo;
	trigger_g = trigger;
	return true;
}
/*La funcion HcSr04Init se encarga de asignar los pines para la lectura del módulo ultrasonido.
 * Siendo echo el impulso recibido por la CIAA que significa la detección de un elemento, y trigger el disparo.
 * Recibe como parametro dos variables tipo gpio_t y devuelve 1 y 0.*/
int16_t HcSr04ReadDistanceCentimeters (void)
 {

uint16_t count = 0;
uint16_t distance =0;
GPIOOn( trigger_g );
DelayUs(10);
GPIOOff( trigger_g);
while (!GPIORead(echo_g))
	{
	}
	while (GPIORead(echo_g))
	{
	DelayUs(1);
	count+=1; /*cada ciclo suma 1 useg*/
	}
distance = count/38; /*u otra medida, calibrar*/
return(distance);
}
/* La funcion HcSr04ReadDistanceCentimeters, cuando es llamada, sensa el dispositivo ultrasonido y acumula e microsegundos el tiempo
 * en que el mismo devuelve una respuesta en alto. No recibe parámetro alguno y devuelve un entero, que es la distancia en cm *
 */

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

