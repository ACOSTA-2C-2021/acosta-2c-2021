/*! @mainpage LDR
 *
 * \section genDesc General Description
 *
 * Driver para LDR_GL5528 se utiliza conectada en serie con una resistencia de calibracion de 47Kohm, con una alimentacion de 5V.
 * Este driver inicializa el ADC,y convierte en valor digital de salida del ADC en LUX.
 *
 * \section hardConn Hardware Connection
 *
 * | LDR GL5528		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		OUT	 	| 		CH1		|
 * | 		3.3V	| 		3.3V	|
 * | 		GND	 	| 		GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/10/2021 | Document creation		                         |
 * | 04/11/2021	| Documento finalizado						     |
 *
 * @author Acosta Azul
 *
 */
/*==================[inclusions]=============================================*/

#include "gpio.h"
#include "chip.h"
#include "ldr_gl5528.h"
#include "pwm_sct.h"
#include "led.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/

/*Datos de la datasheet de la LDR*/

#define LDR_DARK 1000 /*Resistencia en 0 lux, en Kohm*/
#define LDR_10LUX 15 /*Resistencia en 10 lux, aprox en Kohm*/
#define GAMMA 0.7 /*constante gamma de la LDR*/
#define R_CALIBRACION 47 /*Resistenca de calibracion, formando el divisor de tension con LDR, en Kohm*/
#define LUX_NORMAL 500 /*lux normal*/

/*si el DAC me devuelve 0 en luz_aux, tengo lux maxima, si el DAC me devuelve 1023 significa que tengo lux minima.*/

/*==================[internal data declaration]==============================*/
/*variables necesarias*/

uint16_t luz_aux_dig;
uint32_t ilum_lux;
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool LDRInit(){
	analog_input_config Entrada_Luz={CH1,AINPUTS_SINGLE_READ,NULL};
	AnalogInputInit(&Entrada_Luz);
	AnalogInputReadPolling(CH1,&luz_aux_dig);

return true;
}


bool DatoLux(uint32_t *ilum_lux ){

	AnalogInputReadPolling(CH1,&luz_aux_dig);
	*ilum_lux=(uint32_t)((luz_aux_dig*LDR_DARK*10)/(LDR_10LUX*R_CALIBRACION*(1024-luz_aux_dig))); /*el dato digital lo paso en unidades de lux*/
	return TRUE;
}





/*==================[end of file]============================================*/


