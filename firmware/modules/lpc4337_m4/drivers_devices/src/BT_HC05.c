/*! @mainpage BT_HC05
 *
 * \section genDesc General Description
 *
 *  Este driver es para bluetooth HC05. Inicializa uart, envia y lee datos.
 *
 * \section hardConn Hardware Connection
 *
 * | BTHC05			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		RX		| 		TX232	|
 * | 		TX	 	| 		RX232	|
 * | 		5V	 	| 		5V		|
 * | 		GND	 	| 		GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/10/2021 | Document creation		                         |
 * | 04/11/2021	| Documento finalizado						     |
 *
 * @author Acosta Azul
 *
 */
/*==================[inclusions]=============================================*/

#include "gpio.h"
#include "chip.h"
#include "uart.h"
#include "sapi_rtc.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


bool BTInit(void *funcion){

	serial_config App_BT={SERIAL_PORT_P2_CONNECTOR, 9600, funcion};
	UartInit(&App_BT);

return true;
}


bool BTEnviarDatos(rtc_t *Tiempo, uint32_t Lux){
/*Envio fecha y hora*/
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*R");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Tiempo->year,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"/");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Tiempo->month,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"/");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Tiempo->mday,10));
	/*UartSendString(SERIAL_PORT_P2_CONNECTOR, ":");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Tiempo->wday,10));*/
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*");

	UartSendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *) "*r");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Tiempo->hour,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)":");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Tiempo->min,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)":");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Tiempo->sec,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*");
/*Envio cantidad de lux*/
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*A");
	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(Lux,10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR, (uint8_t *)"*");

return true;
}

bool BTLeerDatos(uint8_t *Estado){
/*Leo estado de persiana*/
	UartReadByte(SERIAL_PORT_P2_CONNECTOR, Estado);

return true;
}






/*==================[end of file]============================================*/


