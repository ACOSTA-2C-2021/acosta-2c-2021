/*! @mainpage LDR
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | LDR5528		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PWM		 	| 	TFIL_2		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Acosta Azul
 *
 */
/*==================[inclusions]=============================================*/

#include "gpio.h"
#include "chip.h"
#include "ldr.h"
#include "pwm_sct.h"
#include "led.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/

/*Datos de la hoja de datos de la LDR*/

#define LDR_DARK 1000 /*Resistencia en 0 lux, en Kohm*/
#define LDR_10LUX 15 /*Resistencia en 10 lux, aprox en Kohm*/
#define GAMMA 0.7 /*constante gamma de la LDR*/
#define R_CALIBRACION 47 /*Resistenca de calibracion, formando el divisor de tension, en Kohm*/
#define LUX_NORMAL 500 /*lux normal*/

uint16_t luz_aux_dig;
uint16_t ilum_lux;
/*si el DAC me devuelve 0 en luz_aux, tengo lux maxima, si el DAC me devuelve 1023 significa que tengo lux minima.

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
/*Esta función devuelve verdadero si esta todo ok, lo que hace es inicializar el DAC,
y devolver el dato digital en luz_aux*/

bool LDRInit(analog_input_config *LDR){

	AnalogInputInit(&LDR);
	AnalogInputRead(CH1,&luz_aux_dig);

return true;
}


bool DatoLux(uint16_t *ilum_lux){
	AnalogInputRead(CH1,&luz_aux_dig);
	ilum_lux=((long)luz_aux_dig*LDR_DARK*10)/((long)LDR_10LUX*R_CALIBRACION*(1024-luz_aux_dig)); /*el dato digital lo paso en unidades de lux*/

	/*if(ilum_lux<LUX_NORMAL){

		LedOn(LED_1);
	}
		else {

			LedOff(LED_1);
		}*/
return true;
}




/*==================[end of file]============================================*/


