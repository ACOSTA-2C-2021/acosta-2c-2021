/*! @mainpage goniometro
 *
 * \section genDesc General Description
 *
 * Driver para goniometro, inicializa el CAD y dependiendo el valor de tension digital
 * devuelve un valor de angulo
 *
 * \section hardConn Hardware Connection
 *
 * | POTENCIOMETRO	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	3.3V		|
 * | 	GND		 	| 	GND			|
 * | 	OUT		 	| 	CH1			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021 	| Document creation		                         |
 * |			|							                     |
 *
 * @author Acosta Azul
 *
 */
/*==================[inclusions]=============================================*/
#include"goniometro.h"
#include "analog_io.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/
#define TENSIONEXTIZQ 1.2
#define TENSIONEXTDER 2.1
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
uint16_t vtension;
uint16_t angulo;
/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void GoniometroInit()
{	 analog_input_config mi_potenciometro= {CH1,AINPUTS_SINGLE_READ,NULL};
	AnalogInputInit(&mi_potenciometro);
	AnalogInputReadPolling(CH1,&vtension);

	return true;
}

bool DatoAngulo(uint16_t *angulo){
	AnalogInputReadPolling(CH1,&vtension);
	if(vtension==TENSIONEXTIZQ){
		angulo=0;

	}
	if(vtension==TENSIONEXTDER){
		angulo=75;
	}
	if((vtension<TENSIONEXTIZQ)&&(vtension>TENSIONEXTDER)){
		angulo=(vtension*117)/3.3;
	}
}
/*El potenciómetro está conectado a 3.3V, por regla de 3 en 3.3v tengo 117, 117grados------3.3v
 * 																			xgrados--------vtension
 * 	donde vtension es el valor que me da el conversor CAD*/


/*==================[end of file]============================================*/
