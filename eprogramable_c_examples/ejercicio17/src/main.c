/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:Acosta, Azul
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ejercicio17/inc/main.h"

#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/
#define EJER18
#define CANT 15
uint8_t valores[15]= {234,123,111,101,32,116,211,24,214,100,124,222,1,129,9};
uint8_t promedio=0;
uint16_t acumulador;
uint8_t i;


/*==================[internal functions declaration]=========================*/


int main(void)
{	//el for tienen en común, tanto el ejercicio 17 como el 18, sólo que a el 18 se le agrega un valor más que es el 233
	for(i=0;i<CANT;i++){
							acumulador=acumulador+valores[i];
	}
	printf("%d Acumulador \r\n",acumulador);

	#ifndef EJER18
	#define EJER17
	promedio=(acumulador/CANT);
	printf("%d promedio del ejercicio 17 \r\n",promedio); //promedio del ejercicio 17, 15 valores
	#endif

	#ifdef EJER18
	uint8_t valor_16=233;
	acumulador=acumulador+valor_16;
	promedio=acumulador>>4; //el ejercicio 18 pedía que se calcule el promedio sin utilizar el operador /, esto se puede hacer con el operador >> (corrimeinto o rotacion hacia la derecha)
	// esta operación se puede ver como que a cada desplazamiento se hace una división entre 2, entonces 4 desplazamientos se ve como una division entre 16
	printf("%d promedio del ejercicio 18 \r\n",promedio); //promedio del ejercicio 18, 16 valores
	#endif

	return 0;
}

/*==================[end of file]============================================*/

