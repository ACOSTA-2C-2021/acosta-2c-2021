/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:Acosta, Azul
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ejercicio16/inc/main.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
uint32_t var=0x01020304;
uint8_t var1, var2, var3, var4;

union Test	{
				struct	{
						uint8_t var1_1;
						uint8_t var2_2;
						uint8_t var3_3;
						uint8_t var4_4;
						};

						uint32_t variable_32;
				};
/*==================[internal functions declaration]=========================*/


int main(void)

{	var4=(uint8_t)var;
	var3=(uint8_t)(var>>8);
	var2=(uint8_t)(var>>16);
	var1=(uint8_t)(var>>24);
	printf("variable 4: %d \r\n", var4);
	printf("variable 3: %d \r\n", var3);
	printf("variable 2: %d \r\n", var2);
	printf("variable 1: %d \r\n", var1);
	printf("variable 32 bits: %d \r\n", var);

	union Test A;
A.variable_32=0x01020304;

	printf(" variable 4: %d \r\n", A.var4_4);
	printf(" variable 3: %d \r\n", A.var3_3);
	printf(" variable 2: %d \r\n", A.var2_2);
	printf(" variable 1: %d \r\n", A.var1_1);
	printf(" variable 32 bits: %d \r\n", A.variable_32);

	return 0;
}

/*==================[end of file]============================================*/

