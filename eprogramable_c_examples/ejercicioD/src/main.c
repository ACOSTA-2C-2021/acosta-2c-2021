/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:Acosta, Azul
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ejercicioD/inc/main.h"

#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/

//Objetivo: Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo
//gpioConf_t. La función deberá establecer en qué valor colocar cada bit del dígito BCD e indexando el vector
//anterior operar sobre el puerto y pin que corresponda.
#define OUT 1
#define IN 0

typedef struct
{
	uint8_t port; /*!< GPIO port number */
	uint8_t pin; /*!< GPIO pin number */
	uint8_t dir; /*!< GPIO direction ‘0’ IN; ‘1’ OUT */
} gpioConf_t;



/*==================[internal functions declaration]=========================*/

//Función
void Operar_Puerto_Pin(uint8_t digitoBCD, gpioConf_t *puertopin){
	uint8_t a;
	for(a=0;a<4;a++){
		if(digitoBCD%2==1){
			printf("%d bit puerto:%d.%d:1\r\n", a, puertopin[a].port, puertopin[a].pin);
		}
		else {
			printf("%d bit puerto:%d.%d:1\r\n", a, puertopin[a].port, puertopin[a].pin);
		}
	}

	digitoBCD=digitoBCD/2;
}

int main(void)
{	/*	b0 -> puerto 1.4
		b1 -> puerto 1.5
		b2 -> puerto 1.6
		b3 -> puerto 2.14*/

	gpioConf_t puertos_pines[4]={
			{1,4,OUT},
			{1,5,OUT},
			{1,6,OUT},
			{2,14,OUT},
	};
	uint8_t BCD=4;
	Operar_Puerto_Pin(BCD, puertos_pines);

	return 0;
}

/*==================[end of file]============================================*/
