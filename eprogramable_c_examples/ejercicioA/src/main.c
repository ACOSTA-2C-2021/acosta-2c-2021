/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:Acosta, Azul
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ejercicioA/inc/main.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*==================[macros and definitions]=================================*/

typedef enum {LED_1=1, LED_2=2, LED_3=3}leds_t;
typedef enum {ON=1, OFF=2, TOGGLE=3}modo_t;
typedef struct {	uint8_t n_led;
					uint8_t n_ciclos;
					uint8_t periodo;
					uint8_t mode;
					} my_leds;

void Control_Led(my_leds *mileds)
 {

	uint8_t i=0;
	uint8_t a=0;

	printf("número de led %d \r\n", mileds->n_led);

	switch(mileds->mode){

		case ON:
			switch(mileds->n_led){
									case LED_1:
										printf("%d enciende led \r\n", mileds->n_led);
									break;

									case LED_2:
										printf("%d enciende led \r\n", mileds->n_led);
									break;

									case LED_3:
										printf("%d enciende led \r\n", mileds->n_led);
									break;
									}
			break;

		case OFF:
			switch(mileds->n_led){
									case LED_1:
										printf("%d apaga led \r\n", mileds->n_led);
									break;

									case LED_2:
										printf("%d apaga led \r\n", mileds->n_led);
									break;

									case LED_3:
										printf("%d apaga led \r\n", mileds->n_led);
									break;
									}
			break;

		case TOGGLE:
				for(i=0;i<mileds->n_ciclos;i++){
													switch(mileds->n_led){
													case LED_1:
																printf("Led toogle %d \r\n: ", mileds->n_led);
													break;
													case LED_2:
																printf("Led toogle %d \r\n:", mileds->n_led);
													break;
													case LED_3:
																printf("Led toogle %d \r\n :", mileds->n_led);
													break;
												}

												for(a=0;a<mileds->periodo;a++){
													printf("Retardo de %d \r\n:", mileds->periodo);
												}
												}

	break;
	}
 }



/*==================[internal functions declaration]=========================*/


int main(void)
{ 	my_leds led;
	led.n_led=2;
	led.n_ciclos=2;
	led.periodo=2;
	led.mode=3;


	Control_Led(&led);

//led 2 en modo toogle, 2 ciclos, con retardo 2
	return 0;
}
/*==================[end of file]============================================*/

