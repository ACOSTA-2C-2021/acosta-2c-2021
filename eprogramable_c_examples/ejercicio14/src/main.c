/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:Acosta, Azul
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ejercicio14/inc/main.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
struct Alumno {
	char nombre[12];
	char apellido[20];
	int edad;
				} alumno_1;

struct Alumno *alumno_ptr;
struct Alumno alumno_2;

/*==================[internal functions declaration]=========================*/


int main(void)
{		//item a, mis datos
		strcpy(alumno_1.nombre,"Rocio Azul");
		strcpy(alumno_1.apellido,"Acosta");
		alumno_1.edad=24;
			printf("alumno:\r\n");
			printf("nombre:%s\r\n", alumno_1.nombre);
			printf("apellido:%s\r\n", alumno_1.apellido);
			printf("edad:%d\r\n", alumno_1.edad);

		//item b, datos de mi compañera
		alumno_ptr=&alumno_2;
		strcpy(alumno_ptr->nombre,"Pabla");
		strcpy(alumno_ptr->apellido,"Molina");
		alumno_ptr->edad=30;

			printf("alumno:\r\n");
			printf("nombre:%s \r\n", alumno_ptr->nombre);
			printf("apellido:%s \r\n", alumno_ptr->apellido);
			printf("edad: %d \r\n", alumno_ptr->edad);

	return 0;
}
