/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Acosta Azul
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ejercicioC/inc/main.h"

#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/

uint32_t dato;
uint8_t digitos;

int8_t Binary_To_BCD(uint32_t data, uint8_t digits, uint8_t *bcd_number)
{	uint8_t aux;
	uint8_t a;

	//si data es 124, y le hago el %10, me da el resto, o sea 4
				for(a=digits; a>0; a--){
					aux=data%10;
					bcd_number[(a-1)]=aux; //arranca desde el cero, por eso a-1
					data=data/10; // data/10 me da 12
				}
return 0;}
/*==================[internal functions declaration]=========================*/


int main(void)
{ 	dato=124;
	digitos=3;
	uint8_t vector_de_digitos[digitos];
	Binary_To_BCD(dato, digitos, vector_de_digitos);

	uint8_t i;
	for(i=0;i<digitos;i++){
		printf("%d digitos de salida \r\n",vector_de_digitos[i]);
		}



	return 0;
}

/*==================[end of file]============================================*/

