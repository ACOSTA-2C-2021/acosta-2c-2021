/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:Acosta, Azul
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ejercicio9/inc/main.h"

#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/
const int32_t VALOR_1=28;/*declaro la constante de 32 bit, inicializada en 28, que en numero binario tiene un 1 en el bit 4*/
int32_t variable_a=0;/*variable a iniciliazada en 0*/
const int32_t MSK_1=1<<4;/*máscara que tiene 1 en bit 4, al hacer en main VALOR_1&MSK_1, es una multiplicación lógica, si en el bit 4 de VALOR_1 tengo 0, 0&1=0, si tengo 1, 1&1=1*/

/*==================[internal functions declaration]=========================*/


int main(void)
{


	if((VALOR_1&MSK_1)==0)
							{
								printf("%d es cero\r\n",variable_a);
							}


					else
							{
								variable_a=0xaa;
								printf("%d no es cero\r\n",variable_a);
							}
	return 0;
}

/*==================[end of file]============================================*/

